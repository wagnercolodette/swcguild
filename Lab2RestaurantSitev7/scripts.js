function formValues() {
    // NAME AND ONE FORM OF CONTACT INFORMATION (EMAIL OR PHONE)
    // SHOULD BE FILLED IN.
    var nameCheck = document.getElementById("user-name").value;
    if (nameCheck == "")
        document.getElementById("name").style.color = "red";

    var secondaryContact = document.getElementsByClassName("secondary-contact");
    var secondaryContactCount = 0;
    for (var i = 0; i < secondaryContact.length; i++) {
        if (secondaryContact[0].value != "")
            secondaryContactCount++;
    }
    if (secondaryContactCount == 0) {
        document.getElementById("email").style.color = "red";
        document.getElementById("phone").style.color = "red";
    }

    // IF REASON FOR INQUIRY IS SELECTED TO OTHER, ADDITIONAL INFO
    // SHOULD BE FILLED IN.
    var inquiryCheck = document.getElementById("inquiry-selection").value;
    var textAreaCheck = document.getElementById("text-area").value.length;
    if (inquiryCheck == "Other" && textAreaCheck == 0) {
        document.getElementById("more-info").style.color = "red";
    }

    // BEST DAYS TO CONTACT MUST HAVE AT LEAST ONE DAY CHECKED
    var checkbox = document.getElementsByClassName("checkbox");
    var checkboxCount = 0;
    for (var j = 0; j < checkbox.length; j++) {
        if (checkbox[j].checked == true)
            checkboxCount++;
    }
    if (checkboxCount == 0)
        document.getElementById("contact-days").style.color = "red";

    // IF ANY OF THE AREAS ARE BLANK, DISPLAY POP-UP WINDOW
    // REMIND USER TO FILL IN THESE FIELDS
    alert("Please fill in the highlighted fields." + "\n" + "Thank you");
}
