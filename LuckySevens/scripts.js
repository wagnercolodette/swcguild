// Set Global variables
var userBet;
var totalRolls;
var amountRemaining;
var maxValue;
var highestRoll;


// Initialize Lucky Seven Game
function luckySeven() {
    userBet = Number(document.getElementById('user-bet').value);
    totalRolls = 0;
    amountRemaining = userBet;
    maxValue = amountRemaining;
    highestRoll = 0;


    while (amountRemaining > 0) {
        var dieOne = Math.floor((Math.random() * 6) + 1);
        var dieTwo = Math.floor((Math.random() * 6) + 1);

        if ((dieOne + dieTwo) == 7)
            amountRemaining += 4;
        else
            amountRemaining -= 1;

        if (amountRemaining > maxValue) {
            maxValue = amountRemaining;
            highestRoll = totalRolls;
        }

        totalRolls++;
    }

    return printResults();
}


// Print results to user
function printResults() {
    document.getElementById("table-container").style.display = "block";
    document.getElementById("starting-bet").innerHTML = "<td>" + "$" + userBet + "</td>";
    document.getElementById("before-broke").innerHTML = "<td>" + totalRolls + "</td>";
    document.getElementById("highest-won").innerHTML = "<td>" + "$" + maxValue + "</td>";
    document.getElementById("max-roll").innerHTML = "<td>" + highestRoll + "</td>";
    document.getElementById("btn").style.display = "none";
    document.getElementById("btn-second").style.display = "inline";
}
